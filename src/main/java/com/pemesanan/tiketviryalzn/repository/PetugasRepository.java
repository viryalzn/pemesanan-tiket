package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.Petugas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PetugasRepository extends JpaRepository<Petugas, Integer> {
}
