package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.Kendaraan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KendaraanRepository extends JpaRepository<Kendaraan, Integer> {
}
