package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.Penumpang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenumpangRepository extends JpaRepository<Penumpang, Integer> {
}
