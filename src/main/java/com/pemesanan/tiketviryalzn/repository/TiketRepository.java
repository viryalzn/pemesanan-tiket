package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.dto.tiket.TiketInfo;
import com.pemesanan.tiketviryalzn.dto.tiket.TiketResponseAll;
import com.pemesanan.tiketviryalzn.entity.Tiket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TiketRepository extends JpaRepository<Tiket, Integer> {
    @Query("SELECT new com.pemesanan.tiketviryalzn.dto.tiket.TiketInfo " +
            "(t.kodeTiket, t.noBangku, k.jenisKendaraan.namaJenisKendaraan, k.jenisKelas.namaJenisKelas)" +
            " FROM Tiket t JOIN t.kendaraan k JOIN t.penumpang p WHERE p.id=:id")
    List<TiketInfo> findDetailTiket(@Param("id") Integer id);

    @Query("SELECT new com.pemesanan.tiketviryalzn.dto.tiket.TiketResponseAll " +
            "(t.id, t.kodeTiket, t.noBangku, t.harga, k.jenisKendaraan.namaJenisKendaraan, t.petugas.nama, t.penumpang.nama)" +
            " FROM Tiket t JOIN t.kendaraan k JOIN t.penumpang pn JOIN t.petugas pt")
    List<TiketResponseAll> findAllTiket();
}
