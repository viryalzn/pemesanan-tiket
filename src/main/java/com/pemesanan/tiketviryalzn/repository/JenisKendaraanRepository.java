package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.JenisKendaraan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JenisKendaraanRepository extends JpaRepository<JenisKendaraan, Integer> {
}
