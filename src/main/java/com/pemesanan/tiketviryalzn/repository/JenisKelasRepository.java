package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.JenisKelas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JenisKelasRepository extends JpaRepository<JenisKelas, Integer> {
}
