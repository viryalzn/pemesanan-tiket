package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends JpaRepository<Login, Integer> {
    Optional<Login> findByEmail(String email);
    Optional<Login> findByToken(String token);
    Optional<Login> findByEmailAndPassword(String email, String password);
    Optional<Login> findByPetugasId(Integer id);
    Optional<Login> findByPenumpangId(Integer id);
}
