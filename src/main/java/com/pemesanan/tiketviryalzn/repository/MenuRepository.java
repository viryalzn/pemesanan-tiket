package com.pemesanan.tiketviryalzn.repository;

import com.pemesanan.tiketviryalzn.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {
}
