package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.login.LoginRequest;
import com.pemesanan.tiketviryalzn.dto.login.LoginResponse;
import com.pemesanan.tiketviryalzn.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/login")
@AllArgsConstructor
public class LoginController {

    LoginService loginService;

    @PostMapping
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {
        LoginResponse response = loginService.login(request);
        return ResponseEntity.ok(response);
    }
}
