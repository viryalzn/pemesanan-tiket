package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.kendaraan.KendaraanRequest;
import com.pemesanan.tiketviryalzn.dto.kendaraan.KendaraanResponse;
import com.pemesanan.tiketviryalzn.entity.Kendaraan;
import com.pemesanan.tiketviryalzn.service.KendaraanService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/kendaraan")
@AllArgsConstructor
public class KendaraanController {

    private final KendaraanService kendaraanService;

    @PostMapping
    public ResponseEntity<KendaraanResponse> addKendaraan(@RequestHeader("token") String token, @RequestBody KendaraanRequest request) {
        KendaraanResponse response = kendaraanService.addKendaraan(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<KendaraanResponse> updateKendaraan(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody KendaraanRequest request) {
        KendaraanResponse response = kendaraanService.updateKendaraan(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteKendaraanById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = kendaraanService.deleteKendaraanById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<Kendaraan>> getAllKendaraan(@RequestHeader("token") String token) {
        List<Kendaraan> response = kendaraanService.getAllKendaraan(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<Kendaraan> getKendaraanById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        Kendaraan response = kendaraanService.getKendaraanById(token, id);
        return ResponseEntity.ok(response);
    }
}
