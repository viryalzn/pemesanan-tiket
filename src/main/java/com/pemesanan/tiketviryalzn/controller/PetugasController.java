package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.petugas.PetugasRequest;
import com.pemesanan.tiketviryalzn.dto.petugas.PetugasResponse;
import com.pemesanan.tiketviryalzn.entity.Petugas;
import com.pemesanan.tiketviryalzn.service.PetugasService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/petugas")
@AllArgsConstructor
public class PetugasController {

    private final PetugasService petugasService;

    @PostMapping("/register")
    public ResponseEntity<PetugasResponse> registerPetugas(@Valid @RequestBody PetugasRequest request) {
        PetugasResponse response = petugasService.registerPetugas(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PetugasResponse> updateUser(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody PetugasRequest request) {
        PetugasResponse response = petugasService.updatePetugas(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePetugasById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = petugasService.deletePetugasById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<Petugas>> getAllPetugas(@RequestHeader("token") String token) {
        List<Petugas> response = petugasService.getAllPetugas(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<Petugas> getPetugasById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        Petugas response = petugasService.getPetugasById(token, id);
        return ResponseEntity.ok(response);
    }
}
