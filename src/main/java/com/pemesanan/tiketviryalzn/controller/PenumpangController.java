package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.penumpang.PenumpangRequest;
import com.pemesanan.tiketviryalzn.dto.penumpang.PenumpangResponse;
import com.pemesanan.tiketviryalzn.entity.Penumpang;
import com.pemesanan.tiketviryalzn.service.PenumpangService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("penumpang")
@AllArgsConstructor
public class PenumpangController {

    private final PenumpangService penumpangService;

    @PostMapping("/register")
    public ResponseEntity<PenumpangResponse> registerPenumpang(@Valid @RequestBody PenumpangRequest request) {
        PenumpangResponse response = penumpangService.registerPenumpang(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PenumpangResponse> updateUser(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody PenumpangRequest request) {
        PenumpangResponse response = penumpangService.updatePenumpang(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePenumpangById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = penumpangService.deletePenumpangById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<Penumpang>> getAllPetugas(@RequestHeader("token") String token) {
        List<Penumpang> response = penumpangService.getAllPenumpang(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<Penumpang> getPetugasById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        Penumpang response = penumpangService.getPenumpangById(token, id);
        return ResponseEntity.ok(response);
    }
}
