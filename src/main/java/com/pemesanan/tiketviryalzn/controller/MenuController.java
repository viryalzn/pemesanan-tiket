package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.entity.Menu;
import com.pemesanan.tiketviryalzn.service.MenuService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/menu")
@AllArgsConstructor
public class MenuController {

    private final MenuService menuService;

    @GetMapping
    public ResponseEntity<List<Menu>> getAllMenu() {
        List<Menu> menuList = menuService.getAllMenu();
        return ResponseEntity.ok(menuList);
    }
}
