package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.tiket.TiketDetail;
import com.pemesanan.tiketviryalzn.dto.tiket.TiketRequest;
import com.pemesanan.tiketviryalzn.dto.tiket.TiketResponse;
import com.pemesanan.tiketviryalzn.dto.tiket.TiketResponseAll;
import com.pemesanan.tiketviryalzn.entity.Tiket;
import com.pemesanan.tiketviryalzn.service.TiketService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/tiket")
@AllArgsConstructor
public class TiketController {

    private final TiketService tiketService;

    @PostMapping
    public ResponseEntity<TiketResponse> createTiket(@RequestHeader("token") String token, @RequestBody TiketRequest request) {
        TiketResponse response = tiketService.createTiket(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TiketResponse> updateTiket(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody TiketRequest request) {
        TiketResponse response = tiketService.updateTiket(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTiketById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = tiketService.deleteTiketById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<TiketResponseAll>> getAllTiket() {
        List<TiketResponseAll> response = tiketService.getAllTiket();
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<List<Tiket>> getTiketById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        List<Tiket> response = tiketService.getTiketById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/detail-tiket/{penumpangId}"))
    public ResponseEntity<TiketDetail> getTiketByIdPenumpang(@RequestHeader("token") String token, @PathVariable("penumpangId") Integer penumpangId) {
        TiketDetail response = tiketService.getTiketByIdPenumpang(token, penumpangId);
        return ResponseEntity.ok(response);
    }
}
