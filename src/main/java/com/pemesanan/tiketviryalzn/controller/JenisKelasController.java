package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.jenis_kelas.JenisKelasRequest;
import com.pemesanan.tiketviryalzn.dto.jenis_kelas.JenisKelasResponse;
import com.pemesanan.tiketviryalzn.entity.JenisKelas;
import com.pemesanan.tiketviryalzn.service.JenisKelasService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/jenis-kelas")
@AllArgsConstructor
public class JenisKelasController {

    private final JenisKelasService jenisKelasService;

    @PostMapping
    public ResponseEntity<JenisKelasResponse> addJenisKelas(@RequestHeader("token") String token, @RequestBody JenisKelasRequest request) {
        JenisKelasResponse response = jenisKelasService.addJenisKelas(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JenisKelasResponse> updateJenisKelas(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody JenisKelasRequest request) {
        JenisKelasResponse response = jenisKelasService.updateJenisKelas(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJenisKelasById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = jenisKelasService.deleteJenisKelasById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<JenisKelas>> getAllJenisKelas(@RequestHeader("token") String token) {
        List<JenisKelas> response = jenisKelasService.getAllJenisKelas(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<JenisKelas> getJenisKelasById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        JenisKelas response = jenisKelasService.getJenisKelasById(token, id);
        return ResponseEntity.ok(response);
    }
}
