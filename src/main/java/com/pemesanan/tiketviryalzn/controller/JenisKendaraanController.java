package com.pemesanan.tiketviryalzn.controller;

import com.pemesanan.tiketviryalzn.dto.jenis_kendaraan.JenisKendaraanRequest;
import com.pemesanan.tiketviryalzn.dto.jenis_kendaraan.JenisKendaraanResponse;
import com.pemesanan.tiketviryalzn.entity.JenisKendaraan;
import com.pemesanan.tiketviryalzn.service.JenisKendaraanService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/jenis-kendaraan")
@AllArgsConstructor
public class JenisKendaraanController {

    private final JenisKendaraanService jenisKendaraanService;

    @PostMapping
    public ResponseEntity<JenisKendaraanResponse> addJenisKendaraan(@RequestHeader("token") String token, @RequestBody JenisKendaraanRequest request) {
        JenisKendaraanResponse response = jenisKendaraanService.addJenisKendaraan(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JenisKendaraanResponse> updateJenisKendaraan(@RequestHeader("token") String token, @PathVariable("id") Integer id, @RequestBody JenisKendaraanRequest request) {
        JenisKendaraanResponse response = jenisKendaraanService.updateJenisKendaraan(token, id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJenisKendaraanById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        String response = jenisKendaraanService.deleteJenisKendaraanById(token, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<JenisKendaraan>> getAllJenisKendaraan(@RequestHeader("token") String token) {
        List<JenisKendaraan> response = jenisKendaraanService.getAllJenisKendaraan(token);
        return ResponseEntity.ok(response);
    }

    @GetMapping(("/{id}"))
    public ResponseEntity<JenisKendaraan> getJenisKendaraanById(@RequestHeader("token") String token, @PathVariable("id") Integer id) {
        JenisKendaraan response = jenisKendaraanService.getJenisKendaraanById(token, id);
        return ResponseEntity.ok(response);
    }
}
