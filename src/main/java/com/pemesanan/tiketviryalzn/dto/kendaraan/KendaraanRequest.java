package com.pemesanan.tiketviryalzn.dto.kendaraan;

import lombok.Data;

@Data
public class KendaraanRequest {
    private Integer jenisKendaraanId;
    private String nomorKendaraan;
    private String jamKeberangkatan;
    private Integer jenisKelasId;
}
