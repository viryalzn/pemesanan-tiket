package com.pemesanan.tiketviryalzn.dto.kendaraan;

import com.pemesanan.tiketviryalzn.entity.JenisKelas;
import com.pemesanan.tiketviryalzn.entity.JenisKendaraan;
import lombok.Data;

@Data
public class KendaraanResponse {
    private Integer kendaraanId;
    private JenisKendaraan jenisKendaraan;
    private String nomorKendaraan;
    private String jamKeberangkatan;
    private JenisKelas jenisKelas;
}
