package com.pemesanan.tiketviryalzn.dto.petugas;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PetugasRequest {
    @NotBlank(message = "Nama harus diisi")
    private String nama;
    @NotBlank(message = "Alamat harus diisi")
    private String alamat;
    @NotBlank(message = "Nomor Telepon harus diisi")
    private String nomorTelepon;
    @NotBlank(message = "Email harus diisi")
    private String email;
}
