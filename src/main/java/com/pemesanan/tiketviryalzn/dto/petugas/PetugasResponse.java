package com.pemesanan.tiketviryalzn.dto.petugas;

import lombok.Data;

@Data
public class PetugasResponse {
    private Integer petugasId;
    private String nip;
    private String nama;
    private String alamat;
    private String nomorTelepon;
    private String email;
}
