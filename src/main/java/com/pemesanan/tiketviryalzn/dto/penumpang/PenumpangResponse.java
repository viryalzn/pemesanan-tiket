package com.pemesanan.tiketviryalzn.dto.penumpang;

import lombok.Data;

@Data
public class PenumpangResponse {
    private Integer penumpangId;
    private String nik;
    private String nama;
    private String alamat;
    private Integer umur;
    private String nomorTelepon;
    private String email;
}
