package com.pemesanan.tiketviryalzn.dto.penumpang;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PenumpangRequest {
    @NotBlank(message = "NIK harus diisi")
    private String nik;
    @NotBlank(message = "Nama harus diisi")
    private String nama;
    @NotBlank(message = "ALamat harus diisi")
    private String alamat;
//    @NotBlank(message = "Umur harus diisi")
    private Integer umur;
    @NotBlank(message = "Nomor telepon harus diisi")
    private String nomorTelepon;
    @NotBlank(message = "Email harus diisi")
    private String email;
}
