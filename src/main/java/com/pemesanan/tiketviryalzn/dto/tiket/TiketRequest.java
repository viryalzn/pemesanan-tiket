package com.pemesanan.tiketviryalzn.dto.tiket;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TiketRequest {
    private Integer noBangku;
    private BigDecimal harga;
    private Integer kendaraanId;
    private Integer petugasId;
    private Integer penumpangId;
}
