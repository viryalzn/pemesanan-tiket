package com.pemesanan.tiketviryalzn.dto.tiket;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TiketDetail {
    private String namaPenumpang;
    private String noTelepon;
    private String alamat;
    private List<TiketInfo> listTiket;
}
