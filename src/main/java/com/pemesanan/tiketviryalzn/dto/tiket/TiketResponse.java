package com.pemesanan.tiketviryalzn.dto.tiket;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TiketResponse {
    private Integer tiketId;
    private String kodeTiket;
    private Integer noBangku;
    private BigDecimal harga;
    private String namaKendaraan;
    private String namaPetugas;
    private String namaPenumpang;
}
