package com.pemesanan.tiketviryalzn.dto.tiket;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TiketInfo {
    private String kodeTiket;
    private Integer noBangku;
    private String jenisKendaraan;
    private String jenisKelas;
}
