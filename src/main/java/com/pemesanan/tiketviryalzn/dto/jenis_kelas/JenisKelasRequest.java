package com.pemesanan.tiketviryalzn.dto.jenis_kelas;

import lombok.Data;

@Data
public class JenisKelasRequest {
    private String namaJenisKelas;
}
