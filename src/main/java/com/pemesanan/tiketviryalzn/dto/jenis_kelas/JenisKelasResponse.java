package com.pemesanan.tiketviryalzn.dto.jenis_kelas;

import lombok.Data;

@Data
public class JenisKelasResponse {
    private Integer id;
    private String namaJenisKelas;
}
