package com.pemesanan.tiketviryalzn.dto.login;

import lombok.Data;

import java.util.Date;

@Data
public class LoginResponse {
    private Integer userId;
    private String role;
    private String token;
    private String nama;
    private Date tanggalLogin;
}
