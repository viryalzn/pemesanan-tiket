package com.pemesanan.tiketviryalzn.dto.jenis_kendaraan;

import lombok.Data;

@Data
public class JenisKendaraanRequest {
    private String namaJenisKendaraan;
}
