package com.pemesanan.tiketviryalzn.dto.jenis_kendaraan;

import lombok.Data;

@Data
public class JenisKendaraanResponse {
    private Integer id;
    private String namaJenisKendaraan;
}
