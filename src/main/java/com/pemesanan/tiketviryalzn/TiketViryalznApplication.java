package com.pemesanan.tiketviryalzn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiketViryalznApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiketViryalznApplication.class, args);
	}

}
