package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.entity.Menu;
import com.pemesanan.tiketviryalzn.repository.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MenuService {

    private final MenuRepository menuRepository;

    public List<Menu> getAllMenu() {
        return menuRepository.findAll();
    }
}
