package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.kendaraan.KendaraanRequest;
import com.pemesanan.tiketviryalzn.dto.kendaraan.KendaraanResponse;
import com.pemesanan.tiketviryalzn.entity.JenisKelas;
import com.pemesanan.tiketviryalzn.entity.JenisKendaraan;
import com.pemesanan.tiketviryalzn.entity.Kendaraan;
import com.pemesanan.tiketviryalzn.repository.JenisKelasRepository;
import com.pemesanan.tiketviryalzn.repository.JenisKendaraanRepository;
import com.pemesanan.tiketviryalzn.repository.KendaraanRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class KendaraanService {

    private final KendaraanRepository kendaraanRepository;
    private final JenisKendaraanRepository jenisKendaraanRepository;
    private final JenisKelasRepository jenisKelasRepository;
    private final TokenService tokenService;
    private static final String PETUGAS = "Petugas";
    private static final String INVALID_TOKEN = "Invalid Token";

    public KendaraanResponse addKendaraan(String token, KendaraanRequest request) {
        KendaraanResponse response = new KendaraanResponse();

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            Kendaraan kendaraan = new Kendaraan();

            Optional<JenisKendaraan> optionalJenisKendaraan = jenisKendaraanRepository.findById(request.getJenisKendaraanId());
            Optional<JenisKelas> optionalJenisKelas = jenisKelasRepository.findById(request.getJenisKelasId());

            optionalJenisKendaraan.ifPresent(kendaraan::setJenisKendaraan);
            kendaraan.setNomorKendaraan(request.getNomorKendaraan());
            kendaraan.setJamKeberangkatan(request.getJamKeberangkatan());
            optionalJenisKelas.ifPresent(kendaraan::setJenisKelas);
            kendaraanRepository.save(kendaraan);

            response.setKendaraanId(kendaraan.getId());
            response.setJenisKendaraan(kendaraan.getJenisKendaraan());
            response.setNomorKendaraan(kendaraan.getNomorKendaraan());
            response.setJamKeberangkatan(kendaraan.getJamKeberangkatan());
            response.setJenisKelas(kendaraan.getJenisKelas());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menambahkan data kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public KendaraanResponse updateKendaraan(String token, Integer id, KendaraanRequest request) {
        KendaraanResponse response = new KendaraanResponse();

        Optional<Kendaraan> optionalKendaraan = kendaraanRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            Kendaraan kendaraan = new Kendaraan();

            Optional<JenisKendaraan> optionalJenisKendaraan = jenisKendaraanRepository.findById(request.getJenisKendaraanId());
            Optional<JenisKelas> optionalJenisKelas = jenisKelasRepository.findById(request.getJenisKelasId());

            optionalKendaraan.ifPresent(value -> kendaraan.setId(value.getId()));
            optionalJenisKendaraan.ifPresent(kendaraan::setJenisKendaraan);
            kendaraan.setNomorKendaraan(request.getNomorKendaraan());
            kendaraan.setJamKeberangkatan(request.getJamKeberangkatan());
            optionalJenisKelas.ifPresent(kendaraan::setJenisKelas);
            kendaraanRepository.save(kendaraan);

            response.setKendaraanId(kendaraan.getId());
            response.setJenisKendaraan(kendaraan.getJenisKendaraan());
            response.setNomorKendaraan(kendaraan.getNomorKendaraan());
            response.setJamKeberangkatan(kendaraan.getJamKeberangkatan());
            response.setJenisKelas(kendaraan.getJenisKelas());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk mengubah data kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deleteKendaraanById(String token, Integer id) {
        String response = "";
        Optional<Kendaraan> kendaraan = kendaraanRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas && kendaraan.isPresent()) {
            try {
                kendaraanRepository.deleteById(id);
                response = "Kendaraan berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menghapus data kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public List<Kendaraan> getAllKendaraan(String token) {
        List<Kendaraan> kendaraanList = null;

        if (tokenService.getToken(token).isValid()) {
            kendaraanList = kendaraanRepository.findAll();
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return kendaraanList;
    }

    public Kendaraan getKendaraanById(String token, Integer id) {
        if (tokenService.getToken(token).isValid()) {
            return kendaraanRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }
}
