package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.login.LoginRequest;
import com.pemesanan.tiketviryalzn.dto.login.LoginResponse;
import com.pemesanan.tiketviryalzn.entity.Login;
import com.pemesanan.tiketviryalzn.repository.LoginRepository;
import com.pemesanan.tiketviryalzn.util.JwtToken;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@AllArgsConstructor
public class LoginService {

    private final LoginRepository loginRepository;

    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();

        Optional<Login> optionalLogin = loginRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());

        Login l = new Login();

        if (optionalLogin.isPresent()) {
            l.setId(optionalLogin.get().getId());
            l.setEmail(request.getEmail());
            l.setPassword(request.getPassword());
            l.setToken(JwtToken.getToken(request));
            l.setTanggalLogin(new Date());

            if (optionalLogin.get().getPetugas() != null) {
                l.setPetugas(optionalLogin.get().getPetugas());

                response.setUserId(optionalLogin.get().getPetugas().getId());
                response.setNama(optionalLogin.get().getPetugas().getNama());
                response.setRole("Petugas");
            } else if (optionalLogin.get().getPenumpang() != null) {
                l.setPenumpang(optionalLogin.get().getPenumpang());

                response.setUserId(optionalLogin.get().getPenumpang().getId());
                response.setNama(optionalLogin.get().getPenumpang().getNama());
                response.setRole("Penumpang");
            }

            loginRepository.save(l);
        } else {
            throw new IllegalArgumentException("User tidak ditemukan");
        }

        response.setToken(l.getToken());
        response.setTanggalLogin(l.getTanggalLogin());

        return response;
    }
}
