package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.penumpang.PenumpangRequest;
import com.pemesanan.tiketviryalzn.dto.penumpang.PenumpangResponse;
import com.pemesanan.tiketviryalzn.entity.Login;
import com.pemesanan.tiketviryalzn.entity.Penumpang;
import com.pemesanan.tiketviryalzn.repository.LoginRepository;
import com.pemesanan.tiketviryalzn.repository.PenumpangRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PenumpangService {

    private final PenumpangRepository penumpangRepository;
    private final LoginRepository loginRepository;
    private final TokenService tokenService;
    private static final String INVALID_TOKEN = "Invalid Token";

    public PenumpangResponse registerPenumpang(PenumpangRequest request) {
        PenumpangResponse response = new PenumpangResponse();

        Optional<Login> userExist = loginRepository.findByEmail(request.getEmail());

        if (userExist.isPresent()) {
            throw new IllegalArgumentException("Email sudah digunakan");
        } else {
            Penumpang penumpang = new Penumpang();
            Login login = new Login();

            penumpang.setNik(request.getNik());
            penumpang.setNama(request.getNama());
            penumpang.setUmur(request.getUmur());
            penumpang.setAlamat(request.getAlamat());
            penumpang.setNomorTelepon(request.getNomorTelepon());
            penumpangRepository.save(penumpang);

            login.setEmail(request.getEmail());
            login.setPassword(penumpang.getNik());
            login.setPenumpang(penumpang);
            loginRepository.save(login);

            response.setPenumpangId(penumpang.getId());
            response.setNik(penumpang.getNik());
            response.setNama(penumpang.getNama());
            response.setUmur(penumpang.getUmur());
            response.setAlamat(penumpang.getAlamat());
            response.setEmail(login.getEmail());
            response.setNomorTelepon(penumpang.getNomorTelepon());
        }

        return response;
    }

    public PenumpangResponse updatePenumpang(String token, Integer id, PenumpangRequest request) {
        PenumpangResponse response = new PenumpangResponse();
        String emailUserExist = "";

        Optional<Penumpang> optionalPenumpang = penumpangRepository.findById(id);
        Optional<Login> emailExist = loginRepository.findByEmail(request.getEmail());
        Optional<Login> userExist = loginRepository.findByPenumpangId(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPenumpang = tokenService.getToken(token).getRole().equalsIgnoreCase("Penumpang");

        if (userExist.isPresent()) {
            emailUserExist = userExist.get().getEmail();
        }

        if (emailExist.isPresent() && !emailUserExist.equalsIgnoreCase(request.getEmail())) {
            throw new IllegalArgumentException("Email sudah digunakan");
        } else if (isValidToken && isPenumpang) {
            Penumpang penumpang = new Penumpang();
            Login login = new Login();

            if (optionalPenumpang.isPresent()) {
                penumpang.setId(optionalPenumpang.get().getId());
                penumpang.setNik(request.getNik());
                penumpang.setNama(request.getNama());
                penumpang.setUmur(request.getUmur());
                penumpang.setAlamat(request.getAlamat());
                penumpang.setNomorTelepon(request.getNomorTelepon());
                penumpangRepository.save(penumpang);

                userExist.ifPresent(value -> login.setId(value.getId()));
                login.setEmail(request.getEmail());
                login.setPassword(penumpang.getNik());
                login.setPenumpang(penumpang);
                loginRepository.save(login);

                response.setPenumpangId(penumpang.getId());
                response.setNik(penumpang.getNik());
                response.setNama(penumpang.getNama());
                response.setUmur(penumpang.getUmur());
                response.setAlamat(penumpang.getAlamat());
                response.setEmail(login.getEmail());
                response.setNomorTelepon(penumpang.getNomorTelepon());
            } else {
                throw new IllegalArgumentException("User tidak ditemukan");
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk mengubah data penumpang");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deletePenumpangById(String token, Integer id) {
        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPenumpang = tokenService.getToken(token).getRole().equalsIgnoreCase("Penumpang");

        if (isValidToken && isPenumpang) {
            try {
                Optional<Penumpang> penumpang = penumpangRepository.findById(id);

                penumpang.ifPresent(value ->
                        loginRepository.deleteById(loginRepository.findByPenumpangId(value.getId()).get().getId()));

                penumpangRepository.deleteById(id);

                return "Penumpang berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menghapus data penumpang");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }

    public List<Penumpang> getAllPenumpang(String token) {
        List<Penumpang> penumpangList = null;

        if (tokenService.getToken(token).isValid()) {
            penumpangList = penumpangRepository.findAll();
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return penumpangList;
    }

    public Penumpang getPenumpangById(String token, Integer id) {
        if (tokenService.getToken(token).isValid()) {
            return penumpangRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }
}
