package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.tiket.*;
import com.pemesanan.tiketviryalzn.entity.*;
import com.pemesanan.tiketviryalzn.repository.KendaraanRepository;
import com.pemesanan.tiketviryalzn.repository.PenumpangRepository;
import com.pemesanan.tiketviryalzn.repository.PetugasRepository;
import com.pemesanan.tiketviryalzn.repository.TiketRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TiketService {

    private final TiketRepository tiketRepository;
    private final KendaraanRepository kendaraanRepository;
    private final PenumpangRepository penumpangRepository;
    private final PetugasRepository petugasRepository;
    private final TokenService tokenService;
    private static final String INVALID_TOKEN = "Invalid Token";

    public TiketResponse createTiket(String token, TiketRequest request) {
        TiketResponse response = new TiketResponse();

        String uuid = UUID.randomUUID().toString().replace("-", "");

        boolean isValidToken = tokenService.getToken(token).isValid();

        if (isValidToken) {
            Tiket tiket = new Tiket();

            Optional<Kendaraan> optionalKendaraan = kendaraanRepository.findById(request.getKendaraanId());
            Optional<Penumpang> optionalPenumpang = penumpangRepository.findById(request.getPenumpangId());
            Optional<Petugas> optionalPetugas = petugasRepository.findById(request.getPetugasId());

            tiket.setKodeTiket(uuid.substring(0, 10).toUpperCase());
            tiket.setNoBangku(request.getNoBangku());
            tiket.setHarga(request.getHarga());
            optionalKendaraan.ifPresent(tiket::setKendaraan);
            optionalPenumpang.ifPresent(tiket::setPenumpang);
            optionalPetugas.ifPresent(tiket::setPetugas);
            tiketRepository.save(tiket);

            response.setTiketId(tiket.getId());
            response.setKodeTiket(tiket.getKodeTiket());
            response.setNoBangku(tiket.getNoBangku());
            response.setHarga(tiket.getHarga());
            response.setNamaKendaraan(tiket.getKendaraan().getJenisKendaraan().getNamaJenisKendaraan());
            response.setNamaPenumpang(tiket.getPenumpang().getNama());
            response.setNamaPetugas(tiket.getPetugas().getNama());
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public TiketResponse updateTiket(String token, Integer id, TiketRequest request) {
        TiketResponse response = new TiketResponse();

        Optional<Tiket> optionalTiket = tiketRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();

        if (isValidToken) {
            Tiket tiket = new Tiket();

            Optional<Kendaraan> optionalKendaraan = kendaraanRepository.findById(request.getKendaraanId());
            Optional<Penumpang> optionalPenumpang = penumpangRepository.findById(request.getPenumpangId());
            Optional<Petugas> optionalPetugas = petugasRepository.findById(request.getPetugasId());

            optionalTiket.ifPresent(value -> tiket.setId(value.getId()));
            optionalTiket.ifPresent(value -> tiket.setKodeTiket(value.getKodeTiket()));
            tiket.setNoBangku(request.getNoBangku());
            tiket.setHarga(request.getHarga());
            optionalKendaraan.ifPresent(tiket::setKendaraan);
            optionalPenumpang.ifPresent(tiket::setPenumpang);
            optionalPetugas.ifPresent(tiket::setPetugas);
            tiketRepository.save(tiket);

            response.setTiketId(tiket.getId());
            response.setKodeTiket(tiket.getKodeTiket());
            response.setNoBangku(tiket.getNoBangku());
            response.setHarga(tiket.getHarga());
            response.setNamaKendaraan(tiket.getKendaraan().getJenisKendaraan().getNamaJenisKendaraan());
            response.setNamaPenumpang(tiket.getPenumpang().getNama());
            response.setNamaPetugas(tiket.getPetugas().getNama());
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deleteTiketById(String token, Integer id) {
        String response = "";
        Optional<Tiket> tiket = tiketRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();

        if (isValidToken && tiket.isPresent()) {
            try {
                tiketRepository.deleteById(id);
                response = "Tiket berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public List<TiketResponseAll> getAllTiket() {
        List<TiketResponseAll> tiketList = null;

        tiketList = tiketRepository.findAllTiket();

        return tiketList;
    }

    public List<Tiket> getTiketById(String token, Integer id) {
        Optional<Tiket> tiketOptional = tiketRepository.findById(id);
        List<Tiket> tiket = null;

        if (tokenService.getToken(token).isValid()) {
            if (tiketOptional.isPresent()) {
                tiket = Collections.singletonList(tiketOptional.get());
            }
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return tiket;
    }

    public TiketDetail getTiketByIdPenumpang(String token, Integer penumpangId) {
        Optional<Penumpang> optionalPenumpang = penumpangRepository.findById(penumpangId);
        List<TiketInfo> listTiket = null;

        if (tokenService.getToken(token).isValid()) {
            if (optionalPenumpang.isPresent()) {
                listTiket = tiketRepository.findDetailTiket(penumpangId);
            } else {
                throw new IllegalArgumentException("Penumpang tidak ditemukan");
            }
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return TiketDetail.builder()
                .namaPenumpang(optionalPenumpang.get().getNama())
                .noTelepon(optionalPenumpang.get().getNomorTelepon())
                .alamat(optionalPenumpang.get().getAlamat())
                .listTiket(listTiket)
                .build();
    }
}
