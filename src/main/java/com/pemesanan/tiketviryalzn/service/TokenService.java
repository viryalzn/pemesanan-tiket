package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.token.TokenResponse;
import com.pemesanan.tiketviryalzn.entity.Login;
import com.pemesanan.tiketviryalzn.repository.LoginRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class TokenService {

    private final LoginRepository loginRepository;

    public TokenResponse getToken(String token) {
        TokenResponse response = new TokenResponse();

        Optional<Login> getToken = loginRepository.findByToken(token);
        boolean validate = getToken.isPresent();

        response.setValid(validate);

        if (validate) {
            if (getToken.get().getPenumpang() != null) {
                response.setRole("Penumpang");
            } else if (getToken.get().getPetugas() != null) {
                response.setRole("Petugas");
            }
        }

        return response;
    }
}
