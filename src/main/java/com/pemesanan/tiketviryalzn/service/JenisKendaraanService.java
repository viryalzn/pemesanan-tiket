package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.jenis_kendaraan.JenisKendaraanRequest;
import com.pemesanan.tiketviryalzn.dto.jenis_kendaraan.JenisKendaraanResponse;
import com.pemesanan.tiketviryalzn.entity.JenisKendaraan;
import com.pemesanan.tiketviryalzn.repository.JenisKendaraanRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class JenisKendaraanService {

    private final JenisKendaraanRepository jenisKendaraanRepository;
    private final TokenService tokenService;
    private static final String PETUGAS = "Petugas";
    private static final String INVALID_TOKEN = "Invalid Token";

    public JenisKendaraanResponse addJenisKendaraan(String token, JenisKendaraanRequest request) {
        JenisKendaraanResponse response = new JenisKendaraanResponse();

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            JenisKendaraan jenisKendaraan = new JenisKendaraan();
            jenisKendaraan.setNamaJenisKendaraan(request.getNamaJenisKendaraan());
            jenisKendaraanRepository.save(jenisKendaraan);

            response.setId(jenisKendaraan.getId());
            response.setNamaJenisKendaraan(jenisKendaraan.getNamaJenisKendaraan());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menambahkan data jenis kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public JenisKendaraanResponse updateJenisKendaraan(String token, Integer id, JenisKendaraanRequest request) {
        JenisKendaraanResponse response = new JenisKendaraanResponse();

        Optional<JenisKendaraan> optionalJenisKendaraan = jenisKendaraanRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            JenisKendaraan jenisKendaraan = new JenisKendaraan();
            optionalJenisKendaraan.ifPresent(kendaraan -> jenisKendaraan.setId(kendaraan.getId()));
            jenisKendaraan.setNamaJenisKendaraan(request.getNamaJenisKendaraan());
            jenisKendaraanRepository.save(jenisKendaraan);

            response.setId(jenisKendaraan.getId());
            response.setNamaJenisKendaraan(jenisKendaraan.getNamaJenisKendaraan());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk mengubah data jenis kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deleteJenisKendaraanById(String token, Integer id) {
        String response = "";
        Optional<JenisKendaraan> jenisKendaraan = jenisKendaraanRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas && jenisKendaraan.isPresent()) {
            try {
                jenisKendaraanRepository.deleteById(id);
                response = "Jenis kendaraan berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menghapus data jenis kendaraan");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public List<JenisKendaraan> getAllJenisKendaraan(String token) {
        List<JenisKendaraan> jenisKendaraanList = null;

        if (tokenService.getToken(token).isValid()) {
            jenisKendaraanList = jenisKendaraanRepository.findAll();
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return jenisKendaraanList;
    }

    public JenisKendaraan getJenisKendaraanById(String token, Integer id) {
        if (tokenService.getToken(token).isValid()) {
            return jenisKendaraanRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }
}
