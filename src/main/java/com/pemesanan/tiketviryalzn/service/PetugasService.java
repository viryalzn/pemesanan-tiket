package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.petugas.PetugasRequest;
import com.pemesanan.tiketviryalzn.dto.petugas.PetugasResponse;
import com.pemesanan.tiketviryalzn.entity.Login;
import com.pemesanan.tiketviryalzn.entity.Petugas;
import com.pemesanan.tiketviryalzn.repository.LoginRepository;
import com.pemesanan.tiketviryalzn.repository.PetugasRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@AllArgsConstructor
public class PetugasService {

    private final PetugasRepository petugasRepository;
    private final LoginRepository loginRepository;
    private final TokenService tokenService;
    private static final String INVALID_TOKEN = "Invalid Token";

    private Random random = new Random();

    public PetugasResponse registerPetugas(PetugasRequest request) {
        PetugasResponse response = new PetugasResponse();

        Optional<Login> userExist = loginRepository.findByEmail(request.getEmail());

        if (userExist.isPresent()) {
            throw new IllegalArgumentException("Email sudah digunakan");
        } else {
            Petugas petugas = new Petugas();
            Login login = new Login();

            petugas.setNip(String.format("%08d", this.random.nextInt(99999999)));
            petugas.setNama(request.getNama());
            petugas.setAlamat(request.getAlamat());
            petugas.setNomorTelepon(request.getNomorTelepon());
            petugasRepository.save(petugas);

            login.setEmail(request.getEmail());
            login.setPassword(petugas.getNip());
            login.setPetugas(petugas);
            loginRepository.save(login);

            response.setPetugasId(petugas.getId());
            response.setNip(petugas.getNip());
            response.setNama(petugas.getNama());
            response.setAlamat(petugas.getAlamat());
            response.setEmail(login.getEmail());
            response.setNomorTelepon(petugas.getNomorTelepon());
        }

        return response;
    }

    public PetugasResponse updatePetugas(String token, Integer id, PetugasRequest request) {
        PetugasResponse response = new PetugasResponse();
        String emailUserExist = "";

        Optional<Petugas> optionalPetugas = petugasRepository.findById(id);
        Optional<Login> emailExist = loginRepository.findByEmail(request.getEmail());
        Optional<Login> userExist = loginRepository.findByPetugasId(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase("Petugas");

        if (userExist.isPresent()) {
            emailUserExist = userExist.get().getEmail();
        }

        if (emailExist.isPresent() && !emailUserExist.equalsIgnoreCase(request.getEmail())) {
            throw new IllegalArgumentException("Email sudah digunakan");
        } else if (isValidToken && isPetugas) {
            Petugas petugas = new Petugas();
            Login login = new Login();

            if (optionalPetugas.isPresent()) {
                petugas.setId(optionalPetugas.get().getId());
                petugas.setNip(optionalPetugas.get().getNip());
                petugas.setNama(request.getNama());
                petugas.setAlamat(request.getAlamat());
                petugas.setNomorTelepon(request.getNomorTelepon());
                petugasRepository.save(petugas);

                userExist.ifPresent(value -> login.setId(value.getId()));
                login.setEmail(request.getEmail());
                login.setPassword(petugas.getNip());
                userExist.ifPresent(value -> login.setToken(value.getToken()));
                userExist.ifPresent(value -> login.setTanggalLogin(value.getTanggalLogin()));
                login.setPetugas(petugas);
                loginRepository.save(login);

                response.setPetugasId(petugas.getId());
                response.setNip(petugas.getNip());
                response.setNama(petugas.getNama());
                response.setAlamat(petugas.getAlamat());
                response.setEmail(login.getEmail());
                response.setNomorTelepon(petugas.getNomorTelepon());
            } else {
                throw new IllegalArgumentException("User tidak ditemukan");
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk mengubah data penumpang");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deletePetugasById(String token, Integer id) {
        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase("Petugas");

        if (isValidToken && isPetugas) {
            try {
                Optional<Petugas> petugas = petugasRepository.findById(id);

                petugas.ifPresent(value ->
                        loginRepository.deleteById(loginRepository.findByPetugasId(value.getId()).get().getId()));

                petugasRepository.deleteById(id);

                return "Petugas berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menghapus data petugas");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }

    public List<Petugas> getAllPetugas(String token) {
        List<Petugas> petugasList = null;

        if (tokenService.getToken(token).isValid()) {
            petugasList = petugasRepository.findAll();
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return petugasList;
    }

    public Petugas getPetugasById(String token, Integer id) {
        if (tokenService.getToken(token).isValid()) {
            return petugasRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }
}
