package com.pemesanan.tiketviryalzn.service;

import com.pemesanan.tiketviryalzn.dto.jenis_kelas.JenisKelasRequest;
import com.pemesanan.tiketviryalzn.dto.jenis_kelas.JenisKelasResponse;
import com.pemesanan.tiketviryalzn.entity.JenisKelas;
import com.pemesanan.tiketviryalzn.repository.JenisKelasRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class JenisKelasService {

    private final JenisKelasRepository jenisKelasRepository;
    private final TokenService tokenService;
    private static final String PETUGAS = "Petugas";
    private static final String INVALID_TOKEN = "Invalid Token";

    public JenisKelasResponse addJenisKelas(String token, JenisKelasRequest request) {
        JenisKelasResponse response = new JenisKelasResponse();

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            JenisKelas jenisKelas = new JenisKelas();
            jenisKelas.setNamaJenisKelas(request.getNamaJenisKelas());
            jenisKelasRepository.save(jenisKelas);

            response.setId(jenisKelas.getId());
            response.setNamaJenisKelas(jenisKelas.getNamaJenisKelas());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menambahkan data jenis kelas");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public JenisKelasResponse updateJenisKelas(String token, Integer id, JenisKelasRequest request) {
        JenisKelasResponse response = new JenisKelasResponse();

        Optional<JenisKelas> optionalJenisKelas = jenisKelasRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas) {
            JenisKelas jenisKelas = new JenisKelas();
            optionalJenisKelas.ifPresent(kelas -> jenisKelas.setId(kelas.getId()));
            jenisKelas.setNamaJenisKelas(request.getNamaJenisKelas());
            jenisKelasRepository.save(jenisKelas);

            response.setId(jenisKelas.getId());
            response.setNamaJenisKelas(jenisKelas.getNamaJenisKelas());
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk mengubah data jenis kelas");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public String deleteJenisKelasById(String token, Integer id) {
        String response = "";
        Optional<JenisKelas> jenisKelas = jenisKelasRepository.findById(id);

        boolean isValidToken = tokenService.getToken(token).isValid();
        boolean isPetugas = tokenService.getToken(token).getRole().equalsIgnoreCase(PETUGAS);

        if (isValidToken && isPetugas && jenisKelas.isPresent()) {
            try {
                jenisKelasRepository.deleteById(id);
                response = "Jenis kelas berhasil dihapus";
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else if (isValidToken) {
            throw new IllegalArgumentException("Anda tidak memiliki akses untuk menghapus data jenis kelas");
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return response;
    }

    public List<JenisKelas> getAllJenisKelas(String token) {
        List<JenisKelas> jenisKelasList = null;

        if (tokenService.getToken(token).isValid()) {
            jenisKelasList = jenisKelasRepository.findAll();
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }

        return jenisKelasList;
    }

    public JenisKelas getJenisKelasById(String token, Integer id) {
        if (tokenService.getToken(token).isValid()) {
            return jenisKelasRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
        } else {
            throw new IllegalArgumentException(INVALID_TOKEN);
        }
    }
}
