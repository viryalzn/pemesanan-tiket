package com.pemesanan.tiketviryalzn.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Kendaraan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nomorKendaraan;
    private String jamKeberangkatan;

    @ManyToOne
    private JenisKendaraan jenisKendaraan;

    @ManyToOne
    private JenisKelas jenisKelas;

}
