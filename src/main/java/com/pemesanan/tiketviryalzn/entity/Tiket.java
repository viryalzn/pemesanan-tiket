package com.pemesanan.tiketviryalzn.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class Tiket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String kodeTiket;
    private Integer noBangku;
    private BigDecimal harga;

    @ManyToOne
    @JsonIgnore
    private Kendaraan kendaraan;

    @ManyToOne
    @JsonIgnore
    private Petugas petugas;

    @ManyToOne
    @JsonIgnore
    private Penumpang penumpang;

}
