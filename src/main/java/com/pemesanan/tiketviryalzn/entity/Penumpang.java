package com.pemesanan.tiketviryalzn.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Penumpang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nik;
    private String nama;
    private String alamat;
    private Integer umur;
    private String nomorTelepon;

}
