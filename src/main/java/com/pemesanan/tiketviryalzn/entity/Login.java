package com.pemesanan.tiketviryalzn.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Login {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String email;
    private String password;
    private Date tanggalLogin;

    @Column(length = 500)
    private String token;

    @OneToOne
    private Penumpang penumpang;

    @OneToOne
    private Petugas petugas;

}
